var User = require('./user').User;

module.exports = function(req, res, next){
    req.user = res.locals.user = null;
    if(!req.session.user){
        res.redirect('/login'); //no-session
        console.log('no-session')
    }else{
        User.findById(req.session.user, function(err, user){
            if (err) return next(err);
            if(user){
                req.user = res.locals.user = user;
                next();
            }else{
                res.redirect('/logout'); //bad-session
                console.log('bad-session')
            }
        });
    }
};