var User = require('../libs/user').User;

function Game(player, socket){
    this.id = Math.random()%1000;

    this.turnOfWhite = true;

    this.activePlayer = null;
    this.players = [
        {name: player.name, color: player.color, isCheck: false, socket: socket}
    ]

    this.log = [];
    this.fullLog = [];

    this.piecePosition = {
        a1: 'white rook not-moved',
        b1: 'white knight not-moved',
        c1: 'white bishop not-moved',
        e1: 'white king not-moved',
        d1: 'white queen not-moved',
        f1: 'white bishop not-moved',
        g1: 'white knight not-moved',
        h1: 'white rook not-moved',
        a2: 'white pawn not-moved',
        b2: 'white pawn not-moved',
        c2: 'white pawn not-moved',
        d2: 'white pawn not-moved',
        e2: 'white pawn not-moved',
        f2: 'white pawn not-moved',
        g2: 'white pawn not-moved',
        h2: 'white pawn not-moved',
        a7: 'black pawn not-moved',
        b7: 'black pawn not-moved',
        c7: 'black pawn not-moved',
        d7: 'black pawn not-moved',
        e7: 'black pawn not-moved',
        f7: 'black pawn not-moved',
        g7: 'black pawn not-moved',
        h7: 'black pawn not-moved',
        a8: 'black rook not-moved',
        b8: 'black knight not-moved',
        c8: 'black bishop not-moved',
        e8: 'black king not-moved',
        d8: 'black queen not-moved',
        f8: 'black bishop not-moved',
        g8: 'black knight not-moved',
        h8: 'black rook not-moved'
    };
}

Game.prototype.addPlayer = function(player, socket){
    this.players.push({name: player.name, color: player.color, isCheck: false, socket: socket});
    this.startGame();
};

Game.prototype.startGame = function(){
    if(this.players[0].color === 'white'){
        this.activePlayer = this.players[0];
    } else {
        this.activePlayer = this.players[1];
    }

    this.players[0].socket.emit('start', {player:this.players[0].name, color:this.players[0].color, oponent:this.players[1].name}, this.piecePosition);
    this.players[1].socket.emit('start', {player:this.players[1].name, color:this.players[1].color, oponent:this.players[0].name}, this.piecePosition);
};

Game.prototype.move = function(player, movement){
    var log = this.log;
    var fullLog = this.fullLog;
    function logger(color, movement, isCheck){
        fullLog.push(movement);
        if(movement.type === 'move'){
            var logStr = movement.piece.split(' ').splice(1,1).join() + ' ' + movement.starId  + '-' + movement.endId;
        }else if(movement.type === 'attack'){
            var logStr = movement.piece.split(' ').splice(1,1).join() + ' ' + movement.starId  + 'X' + movement.endId;
        }else if(movement.type === 'castle'){
            if(movement.castleType === 'long'){
                var logStr = '0-0-0';
            }else{
                var logStr = '0-0';
            }
        }
        if(isCheck){ logStr += '+'}
        if(color === 'white'){
            log.push(logStr);
        }else{
            log.push(log.pop() + ', ' + logStr);
        }
    }

    if(eqv(player.color === 'white', this.turnOfWhite))
    {
        if(movement.type === 'castle'){
            if(player.color === 'white'){
                if(movement.castleType === 'long'){
                    delete this.piecePosition['e1'];
                    this.piecePosition['c1'] = 'white king';
                    delete this.piecePosition['a1'];
                    this.piecePosition['d1'] = 'white rook';
                }else{
                    delete this.piecePosition['e1'];
                    this.piecePosition['g1'] = 'white king';
                    delete this.piecePosition['h1'];
                    this.piecePosition['f1'] = 'white rook';
                }
            }else{
                if(movement.castleType === 'long'){
                    delete this.piecePosition['e8'];
                    this.piecePosition['c8'] = 'black king';
                    delete this.piecePosition['a8'];
                    this.piecePosition['d8'] = 'black rook';
                }else{
                    delete this.piecePosition['e8'];
                    this.piecePosition['g8'] = 'black king';
                    delete this.piecePosition['h8'];
                    this.piecePosition['f8'] = 'black rook';
                }
            }
        }else{
            delete this.piecePosition[movement.starId];
            this.piecePosition[movement.endId] = movement.piece;
        }

        this.turnOfWhite = !this.turnOfWhite;
        this.changeActivePlayer();

        var kingId = getKingId(this.activePlayer.color, this.piecePosition);
        var isCheck = !!attackVector(kingId, this.activePlayer.color, this.piecePosition);

        this.activePlayer.isCheck = isCheck;

        //TODO: checkmate and stalemate revision

        if(this.isCheckmate()){
            this.activePlayer.socket.emit('loss', 'Шах и мат! Вы проиграли');
            this.activePlayer.socket.disconnect();

            User.update({username: this.activePlayer.name}, {$inc:{ defeat: 1}}, function (err, user) {
                if (err) return handleError(err);
            });

            this.changeActivePlayer();
            User.update({username: this.activePlayer.name}, {$inc:{ victory: 1}}, function (err, user) {
                if (err) return handleError(err);
            });
            this.changeActivePlayer();
        }

        logger(player.color, movement, isCheck);
        this.players[0].socket.emit('move', movement, this.turnOfWhite, isCheck, log);
        this.players[1].socket.emit('move', movement, this.turnOfWhite, isCheck, log);
    }
}

Game.prototype.undoMove = function(player){
    var log = this.log;
    var fullLog = this.fullLog;
    if(fullLog.length > 0)
    {
        function unLog(color, movement, isCheck){
            if(color === 'white'){
                log.pop();
            }else{
                var logStr = log.pop().split(',')[0];
                log.push(logStr);
            }
        }

        if(eqv(player.color === 'black', this.turnOfWhite))
        {
            var movement = fullLog.pop();

            if(movement.type === 'attack'){
                this.piecePosition[movement.endId] = movement.attackedPiece;
                this.piecePosition[movement.starId] = movement.piece;
            }else if(movement.type === 'move'){
                delete this.piecePosition[movement.endId];
                this.piecePosition[movement.starId] = movement.piece;
            }else if(movement.type === 'castle'){
                if(movement.color === 'white'){
                    if(movement.castleType === 'long'){
                        delete this.piecePosition['c1'];
                        this.piecePosition['e1'] = 'white king';
                        delete this.piecePosition['d1'];
                        this.piecePosition['a1'] = 'white rook';
                    }else{
                        delete this.piecePosition['g1'];
                        this.piecePosition['e1'] = 'white king';
                        delete this.piecePosition['f1'];
                        this.piecePosition['h1'] = 'white rook';
                    }
                }else{
                    if(movement.castleType === 'long'){
                        delete this.piecePosition['c8'];
                        this.piecePosition['e8'] = 'black king';
                        delete this.piecePosition['d8'];
                        this.piecePosition['a8'] = 'black rook';
                    }else{
                        delete this.piecePosition['g8'];
                        this.piecePosition['e8'] = 'black king';
                        delete this.piecePosition['f8'];
                        this.piecePosition['h8'] = 'black rook';
                    }
                }
            }

            this.turnOfWhite = !this.turnOfWhite;
            this.changeActivePlayer();

            var kingId = getKingId(this.activePlayer.color, this.piecePosition);
            var isCheck = !!attackVector(kingId, this.activePlayer.color, this.piecePosition);

            this.activePlayer.isCheck = isCheck;

            unLog(player.color, movement, isCheck);
            this.players[0].socket.emit('undoMove', movement, this.turnOfWhite, isCheck, log);
            this.players[1].socket.emit('undoMove', movement, this.turnOfWhite, isCheck, log);
        }
    }
}

Game.prototype.getValidMoves = function (startId, piece, color){
    var pos = getCoordinates(startId);

    var game = this;

    var kingId = getKingId(game.activePlayer.color, game.piecePosition);
    var underAttack = attackVector(kingId, game.activePlayer.color, game.piecePosition);
    var isCheck = !!underAttack;

    var validForMoveCells = [];
    var validForAttackCells = [];
    var validForCastleCells = [];
    var validMoves = {};

    var newPosition = {};

    var position = this.piecePosition;

    if(piece === 'pawn'){
        if(color === 'white'){
            //move
            newPosition = {x: pos.x, y: pos.y - 1};
            validForMoveCells.push(culcId(newPosition));

            if(position[startId].indexOf('not-moved') + 1){
                newPosition = {x: pos.x, y: pos.y - 2};
                validForMoveCells.push(culcId(newPosition));
            }

            //attack
            var id = culcId({x: pos.x + 1, y: pos.y - 1});
            if(id in position){
                validForAttackCells.push(id);
            }

            id = culcId({x: pos.x - 1, y: pos.y - 1});
            if(id in position){
                validForAttackCells.push(id);
            }
        }else{
            //move
            newPosition = {x: pos.x, y: pos.y + 1};
            validForMoveCells.push(culcId(newPosition));

            if(position[startId].indexOf('not-moved') + 1){
                newPosition = {x: pos.x, y: pos.y + 2};
                validForMoveCells.push(culcId(newPosition));
            }

            //attack
            id = culcId({x: pos.x + 1, y: pos.y + 1});
            if(id in position){
                validForAttackCells.push(id);
            }

            id = culcId({x: pos.x - 1, y: pos.y + 1});
            if(id in position){
                validForAttackCells.push(id);
            }
        }
    }

    if(piece === 'knight'){
        newPosition = {x: pos.x + 1, y: pos.y - 2};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x - 1, y: pos.y - 2};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x + 1, y: pos.y + 2};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x - 1, y: pos.y + 2};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x - 2, y: pos.y + 1};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x - 2, y: pos.y - 1};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x + 2, y: pos.y + 1};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        newPosition = {x: pos.x + 2, y: pos.y - 1};
        if(culcId(newPosition) != -1) validForMoveCells.push(culcId(newPosition));

        validForMoveCells.forEach(function(id){
            if(id in position){
                validForAttackCells.push(id);
            }
        });
    }

    if(piece === 'rook' || piece === 'queen'){
        for(var i = 1; pos.x - i  >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.x + i <= 7; i++) {
            id = culcId({x: pos.x + i, y: pos.y});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.y - i >= 0; i++) {
            id = culcId({x: pos.x, y: pos.y - i});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.y + i <= 7; i++){
            id = culcId({x: pos.x, y: pos.y + i});
            if(!position[id]){
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }
    }

    if(piece === 'bishop' || piece === 'queen'){
        for(i = 1; pos.x - i  >= 0 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y - i});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.x - i  >= 0 && pos.y + i <= 7; i++) {
            id = culcId({x: pos.x - i, y: pos.y + i});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.x + i  >= 0 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x + i, y: pos.y - i});
            if (!position[id]) {
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }

        for(i = 1; pos.x + i  <= 7 && pos.y + i <= 7; i++){
            id = culcId({x: pos.x + i, y: pos.y + i});
            if(!position[id]){
                validForMoveCells.push(id);
            }else {
                validForAttackCells.push(id);
                break;
            }
        }
    }

    if(piece === 'king'){
        for(i = -1; i <= 1; i++)
            for(var j = -1; j <= 1; j++){
                var id = culcId({x: pos.x + i, y: pos.y + j});
                if(id != -1 ){
                    if(!position[id]){
                        if(!attackVector(id, game.activePlayer.color, position)) {
                            validForMoveCells.push(id);
                        }
                    }else{
                        if(!attackVector(id, game.activePlayer.color, position)) {
                            validForAttackCells.push(id);
                        }
                    }
                }
            }
        //TODO: castle move
        if(!isCheck){
            function isLongCastle(){
                if(position[culcId(pos)].indexOf('not-moved') + 1) {
                    for (i = 1; i <= 5; i++) {
                        if (game.activePlayer.color === 'white') {
                            var id = culcId({x: pos.x - i, y: pos.y});
                        } else {
                            var id = culcId({x: pos.x + i, y: pos.y});
                        }
                        if (id != -1) {
                            if (position[id] && (position[id].indexOf('rook not-moved') + 1)) {
                                return true;
                            }
                        } else {
                            break;
                        }
                    }
                }
                return false;
            }
            function isShortCastle(){
                if(position[culcId(pos)].indexOf('not-moved') + 1)
                {
                    for(i = 1; i <= 4; i++){
                        if(game.activePlayer.color === 'white'){
                            var id = culcId({x: pos.x + i, y: pos.y});
                        }else{
                            var id = culcId({x: pos.x - i, y: pos.y});
                        }
                        if(id != -1 ) {
                            if (position[id] && (position[id].indexOf('rook not-moved') + 1)) {
                                return true;
                            }
                        } else {
                            break;
                        }
                    }
                }
                return false;
            }

            if(isLongCastle()){
                if(game.activePlayer.color === 'white'){
                    var id = culcId({x: pos.x - 2, y: pos.y});
                    var jumpedCellId = culcId({x: pos.x - 1, y: pos.y});
                } else {
                    var id = culcId({x: pos.x + 2, y: pos.y});
                    var jumpedCellId = culcId({x: pos.x + 1, y: pos.y});
                }
                if(!attackVector(jumpedCellId, game.activePlayer.color, position) && !attackVector(id, game.activePlayer.color, position) )
                    validForCastleCells.push(id);
            }
            if(isShortCastle()){
                if(game.activePlayer.color === 'white'){
                    var id = culcId({x: pos.x + 2, y: pos.y});
                    var jumpedCellId = culcId({x: pos.x + 1, y: pos.y});
                } else {
                    var id = culcId({x: pos.x - 2, y: pos.y});
                    var jumpedCellId = culcId({x: pos.x - 1, y: pos.y});
                }
                if(!attackVector(jumpedCellId, game.activePlayer.color, position) && !attackVector(id, game.activePlayer.color, position) )
                    validForCastleCells.push(id);
            }
        }
    }

    validForMoveCells.forEach(function(element){
        if(!position[element]){//
            if(!(isCheck && !(underAttack.indexOf(element)+1)) || piece === 'king'){//если король под шахом то проверяем защитит ли этот ход короля
                var movement = {startId: startId, endId: element, piece: game.activePlayer.color + ' ' + piece};
                if(!game.isCheckAfterMove(movement)){//проверяем будет ли шах после даного хода
                    validMoves[element] = 'valid-move';
                }
            }
        }
    });

    validForCastleCells.forEach(function(element){
        if(!position[element]){
            if(piece === 'king'){
                validMoves[element] = 'valid-castle';
            }
        }
    });

    validForAttackCells.forEach(function(element){
        if(!(position[element].indexOf(game.activePlayer.color) + 1)){
            if(!(isCheck && !(underAttack.indexOf(element)+1)) || piece === 'king'){
                validMoves[element] = 'valid-attack';
            }
        }
    });

    return validMoves;
}

Game.prototype.changeActivePlayer = function (){
    if(this.players[0] === this.activePlayer) {
        this.activePlayer = this.players[1];
    }
    else{
        this.activePlayer = this.players[0];
    }
}

/**
 * Check is it checkmate on a board
 *  
 * @returns {boolean}
 */
Game.prototype.isCheckmate = function(){
    const pieces = getPieceByColor(this.piecePosition, this.activePlayer.color);

    for(let piece in pieces) {
        const [color, type] = piece.split(' ');
        if(Object.keys(this.getValidMoves(index, type, color)).length === 0) {
            return false;
        }
    }
    return true;
}

Game.prototype.isCheckAfterMove = function(movement){
    var nextPosition = {};
    for(var key in this.piecePosition){
        nextPosition[key] = this.piecePosition[key];
    }

    delete nextPosition[movement.startId];
    nextPosition[movement.endId] = movement.piece;

    var kingId = getKingId(this.activePlayer.color, nextPosition);
    return !!attackVector(kingId, this.activePlayer.color, nextPosition);
};

var cellsId = [ 'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8',
    'a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7',
    'a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6',
    'a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5',
    'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4',
    'a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3',
    'a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2',
    'a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1']

function getPieceByColor(position, color){
    var pieces = {};
    for(var id in position){
        if(position[id].indexOf(color) + 1) pieces[id] = position[id];
    }
    return pieces;
}

function eqv(arg1, arg2){ //еквиваленция
    if(arg1 === true && arg2 === true || arg1 === false && arg2 === false) return true;
    return false;
}

function attackVector(cellId, color, position){
    var pos = getCoordinates(cellId);

    var cellsUnderAttack = []

    var state = false;

    var oponentColor;
    if(color === 'white') oponentColor = 'black';
    else oponentColor = 'white';

    function isUnderPawnAttack(){
        var positionForCheck = [];
        if(color === 'white'){
            positionForCheck.push(culcId({x: pos.x + 1, y: pos.y - 1}));
            positionForCheck.push(culcId({x: pos.x - 1, y: pos.y - 1}));

            positionForCheck.forEach(function(id){
                if(id in position && (position[id].indexOf('black pawn') + 1)) cellsUnderAttack.push(id);
            });
        }else{
            positionForCheck.push(culcId({x: pos.x + 1, y: pos.y + 1}));
            positionForCheck.push(culcId({x: pos.x - 1, y: pos.y + 1}));

            positionForCheck.forEach(function(id){
                if(id in position && (position[id].indexOf('white pawn') + 1)) cellsUnderAttack.push(id);
            });
        }
    }
    function isUnderKnightAttack(){
        var positionForCheck = [];

        positionForCheck.push(culcId({x: pos.x + 2, y: pos.y - 1}));
        positionForCheck.push(culcId({x: pos.x + 2, y: pos.y + 1}));
        positionForCheck.push(culcId({x: pos.x - 2, y: pos.y - 1}));
        positionForCheck.push(culcId({x: pos.x - 2, y: pos.y + 1}));
        positionForCheck.push(culcId({x: pos.x + 1, y: pos.y - 2}));
        positionForCheck.push(culcId({x: pos.x - 1, y: pos.y - 2}));
        positionForCheck.push(culcId({x: pos.x + 1, y: pos.y + 2}));
        positionForCheck.push(culcId({x: pos.x - 1, y: pos.y + 2}));

        positionForCheck.forEach(function(id){
            if(id in position && (position[id].indexOf(oponentColor +' knight') + 1)) cellsUnderAttack.push(id);
        });
    }
    function isUnderRookAttack(){
        var id;
        var positionForCheck = [];
        for(var i = 1; pos.x - i  >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' rook') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7; i++) {
            id = culcId({x: pos.x + i, y: pos.y});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' rook') + 1))
                    positionForCheck.forEach(function(element){
                    cellsUnderAttack.push(element)
                });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.y - i >= 0; i++) {
            id = culcId({x: pos.x, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' rook') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.y + i <= 7; i++){
            id = culcId({x: pos.x, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' rook') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }
    }
    function isUnderBishopAttack(){
        var id;

        var positionForCheck = [];
        for(var i = 1;  pos.x - i  >= 0 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' bishop') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1;  pos.x - i  >= 0 && pos.y + i <= 7; i++) {
            id = culcId({x: pos.x - i, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' bishop') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x + i, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' bishop') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7 && pos.y + i <= 7; i++){
            id = culcId({x: pos.x + i, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' bishop') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }
    }
    function isUnderQueenAttack(){
        var id;
        var positionForCheck = [];

        for(var i = 1; pos.x - i  >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7; i++) {
            id = culcId({x: pos.x + i, y: pos.y});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.y - i >= 0; i++) {
            id = culcId({x: pos.x, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.y + i <= 7; i++){
            id = culcId({x: pos.x, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(var i = 1; pos.x - i  >= 0 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x - i, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x - i  >= 0 && pos.y + i <= 7; i++) {
            id = culcId({x: pos.x - i, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7 && pos.y - i >= 0; i++) {
            id = culcId({x: pos.x + i, y: pos.y - i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }

        positionForCheck = [];
        for(i = 1; pos.x + i <= 7 && pos.y + i <= 7; i++){
            id = culcId({x: pos.x + i, y: pos.y + i});
            positionForCheck.push(id);
            if (position[id]) {
                if(id in position && (position[id].indexOf(oponentColor +' queen') + 1))
                    positionForCheck.forEach(function(element){
                        cellsUnderAttack.push(element)
                    });
                break;
            }
        }
    }
    function isUnderKingAttack(){
        var id;

        for(var i = -1; i <= 1; i++)
            for(var j = -1; j <= 1; j++){
                id = culcId({x: pos.x + i, y: pos.y + j});
                    if(position[id]){
                        if(id in position && (position[id].indexOf(oponentColor +' king') + 1)) cellsUnderAttack.push(id);
                    }
            }
    }

    isUnderPawnAttack();
    isUnderKnightAttack();
    isUnderRookAttack();
    isUnderBishopAttack();
    isUnderQueenAttack();
    isUnderKingAttack();

    if(cellsUnderAttack.length > 0){
        return cellsUnderAttack;
    }
    else{
        return null;
    }
}

function getKingId(color, position){
    for(var id in position){
        if(position[id].indexOf(color + ' king') + 1) return id;
    }
    return null;
}

function getCoordinates(id){
    var index = cellsId.indexOf(id);
    var x = index%8;
    var y = Math.floor(index/8);
    return{x:x, y:y};
}

function culcId(pos){
    if(pos.x > 7 || pos.x < 0) return -1;
    if(pos.y > 7 || pos.y < 0) return -1;
    return cellsId[pos.y*8 + pos.x];
}

module.exports = Game;