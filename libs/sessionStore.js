var session = require('express-session');
var MongoDBStore  = require('connect-mongodb-session')(session);
var mongoose = require('mongoose');

var sessionStore = new MongoDBStore({
    uri: 'mongodb+srv://chess_admin:chess_admin@cluster0.b8l4t.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    collection: 'sessions'
});

module.exports = sessionStore;