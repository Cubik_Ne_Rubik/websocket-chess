var cookie = require('cookie');
var User = require('../libs/user').User;
var sessionStore = require('./sessionStore');
var cookieParser = require('cookie-parser');
var Game = require('./Game');

module.exports = function (server){
    var io = require('socket.io').listen(server);
    var queue = [];
    var games = {};
    var log = [];

    io.set('authorization', function(handshakeData,callback){
        handshakeData.cookies = cookie.parse(handshakeData.headers.cookie || '');
        var sidCookie = handshakeData.cookies['connect.sid'];
        var sid = cookieParser.signedCookie(sidCookie, 'secretstr');
        sessionStore.load(sid, function(err, session){
            if(!session) {
                return callback(new Error(401, "No session"));
            }
            handshakeData.color = session.color;
            User.findById(session.user, function(err, user){
                if(!user) {
                    return callback(new Error(403, "Forbidden"));
                }
                handshakeData.user = user;
                callback(null, true);
            });
        });
    });

    io.sockets.on('connection', function (socket){
        var player = {
            name: socket.request.user.get('username'),
            color: socket.request.color
        };

        var gameId;

        queue.push(socket);

        socket.emit('connection');

        for( var i in queue){
            if(player.color != queue[i].request.color)
            {
                var game = new Game(player,socket);
                games[game.id] = game;

                gameId = game.id;
                queue[i].emit('gameFind', game.id);

                queue.splice(i, 1);
                queue.splice(queue.indexOf(socket), 1);
            }
        }

        socket.on('gameFind', function(Id){
            games[Id].addPlayer(player, socket);
            gameId = Id;
        });

        socket.on('move', function(movement){
            games[gameId].move(player, movement);
        });

        socket.on('undoMove', function(){
            games[gameId].undoMove(player);
        });

        socket.on('validMove', function(id, piece, color, callback){
            var validMoves = games[gameId].validMove(id, piece, color);
            callback(validMoves);
        });

        socket.on('disconnect', function(){
            if(games[gameId]){
                if(games[gameId].players[0].socket === socket){
                    User.update({username: games[gameId].players[1].name}, {$inc:{ victory: 1}}, function (err, user) {
                        if (err) return handleError(err);
                    });

                    games[gameId].players[1].socket.emit('loss', 'Противник отключился! Вы победили!')
                    games[gameId].players[1].socket.disconnect();
                } else {
                    User.update({username: games[gameId].players[0].name}, {$inc:{ defeat: 1}}, function (err, user) {
                        if (err) return handleError(err);
                    });

                    games[gameId].players[0].socket.emit('loss', 'Противник отключился! Вы победили!')
                    games[gameId].players[0].socket.disconnect();
                }
                delete  games[gameId];
            } else {
                queue.splice(queue.indexOf(socket), 1);
            }
        });
    });
};



