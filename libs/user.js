var mongoose = require('./mongooseConnection');
var Schema = mongoose.Schema;

var model = new Schema({
    username:{
        type: String,
        unique: true,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    victory: {
        type: Number
    },
    defeat: {
        type: Number
    },
    time: {
        type: String
    }
});

model.methods.checkPassword = function(password){
    return password === this.password;
};

model.methods.won = function(){
    this.victory++;
    this.update(function (err){
        if (err) return callback(err);
    });
};
model.methods.loss = function(){
    this.defeat++;
    this.update(function (err){
        if (err) return callback(err);
    });
};


model.statics.authorize = function(username, password, callback){
    var User = this;
    User.where({username: username}).findOne(function(err, user){
        if (err) {
            var err = new Error('Username failed');
            err.status = 403;
            callback(err, null);
        }
        if (user) {
            if (user.checkPassword(password)) {
                callback(null, user);
            } else {
                var err = new Error('Password failed');
                err.status = 403;
                callback(err,null);
            }
        }
    });
};

model.statics.registration = function(username, password, callback){
    var User = this;
    var user = new User({username: username, password: password, victory: 0, defeat: 0});
    user.save(function (err){
        if (err) return callback(err);
        callback(null, user);
    });
};

exports.User = mongoose.model('User', model);