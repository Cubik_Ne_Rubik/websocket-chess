var socket = io.connect('');
var board = $('#gameboard');
var boardId = [ 'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8',
                'a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7',
                'a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6',
                'a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5',
                'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4',
                'a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3',
                'a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2',
                'a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1']
var playerColor;

$(document).ready(function() {
    $('#modal_close, #overlay').click( function(){
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
            function(){
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
    });

    $('#undoMove').click(function(){
        socket.emit('undoMove');
    })
});

socket.on('connection', function() {
    socket.on('start', function (gameData, startPosition) {
        playerColor = gameData.color;
        if (playerColor === 'white') {
            $('.cell').each(function (index) { //set board
                var id = '';
                switch (index % 8) {
                    case 0:
                        id = 'a';
                        break;
                    case 1:
                        id = 'b';
                        break;
                    case 2:
                        id = 'c';
                        break;
                    case 3:
                        id = 'd';
                        break;
                    case 4:
                        id = 'e';
                        break;
                    case 5:
                        id = 'f';
                        break;
                    case 6:
                        id = 'g';
                        break;
                    case 7:
                        id = 'h';
                        break;
                }
                id += (8 - Math.floor(index / 8));
                $(this).attr('id', id);
            });
            var setHBorder = function (index) {
                var text = '';
                switch (index % 8) {
                    case 0:
                        text = 'a';
                        break;
                    case 1:
                        text = 'b';
                        break;
                    case 2:
                        text = 'c';
                        break;
                    case 3:
                        text = 'd';
                        break;
                    case 4:
                        text = 'e';
                        break;
                    case 5:
                        text = 'f';
                        break;
                    case 6:
                        text = 'g';
                        break;
                    case 7:
                        text = 'h';
                        break;
                }
                $(this).text(text);
            }
            var setVBorder = function (index) {
                var text = (8 - index);
                $(this).text(text);
            }
            $('.top-border').each(setHBorder);
            $('.bottom-border').each(setHBorder);

            $('.left-border').each(setVBorder);
            $('.right-border').each(setVBorder);
            $('#player-info').empty().text('Ваш ход!');
        } else if (playerColor === 'black') {
            boardId.reverse();
            $('.cell').each(function (index) { //set board
                var id = '';
                switch (index % 8) {
                    case 0:
                        id = 'h';
                        break;
                    case 1:
                        id = 'g';
                        break;
                    case 2:
                        id = 'f';
                        break;
                    case 3:
                        id = 'e';
                        break;
                    case 4:
                        id = 'd';
                        break;
                    case 5:
                        id = 'c';
                        break;
                    case 6:
                        id = 'b';
                        break;
                    case 7:
                        id = 'a';
                        break;
                }
                id += (Math.floor(index / 8) + 1);
                $(this).attr('id', id);
            });
            var setHBorder = function (index) {
                var text = '';
                switch (index % 8) {
                    case 0:
                        text = 'h';
                        break;
                    case 1:
                        text = 'g';
                        break;
                    case 2:
                        text = 'f';
                        break;
                    case 3:
                        text = 'e';
                        break;
                    case 4:
                        text = 'd';
                        break;
                    case 5:
                        text = 'c';
                        break;
                    case 6:
                        text = 'b';
                        break;
                    case 7:
                        text = 'a';
                        break;
                }
                $(this).text(text);
            }
            var setVBorder = function (index) {
                var text = (index + 1);
                $(this).text(text);
            }
            $('.top-border').each(setHBorder);
            $('.bottom-border').each(setHBorder);

            $('.left-border').each(setVBorder);
            $('.right-border').each(setVBorder);
            $('#player-info').empty().text('Ход противника!');
        }

        for (var key in startPosition) {
            $('#' + key).removeClass('empty').addClass(startPosition[key]);
        }

        board.on('click', '.' + playerColor, function () {
            var isSelected = $(this).hasClass('selected')

            clearSelections();

            if (!isSelected) {
                var piece = $(this).addClass('selected').attr('class').split(' ').slice(2, 3).join();
                socket.emit('validMove', $(this).attr('id'), piece, playerColor, function (validMove) {
                    for (var key in validMove) {
                        $('#' + key).addClass(validMove[key]);
                    }
                });
            }

        });

        board.on('click', '.valid-move', function () {
            var piece = $('.selected').attr('class').split(' ').slice(1, 3).join(' ');
            var movement = {
                starId: $('.selected').attr('id'),
                endId: $(this).attr('id'),
                piece: piece,
                type: 'move'
            }
            socket.emit('move', movement);
            clearSelections();
        });

        board.on('click', '.valid-attack', function () {
            var piece = $('.selected').attr('class').split(' ').slice(1, 3).join(' ');
            var attackedPiece = $(this).attr('class').split(' ').slice(1, 3).join(' ');
            var attackData = {
                starId: $('.selected').attr('id'),
                endId: $(this).attr('id'),
                piece: piece,
                attackedPiece: attackedPiece,
                type: 'attack'
            }
            socket.emit('move', attackData);
            clearSelections();
        });

        board.on('click', '.valid-castle', function () {
            var id = $(this).attr('id');
            if(id.indexOf('c') + 1){
                var  type = 'long';
            }else{
                var  type = 'short';
            }
            var movement = {
                color: playerColor,
                castleType: type,
                type: 'castle'
            }
            socket.emit('move', movement);
            clearSelections();
        });

        board.on('click', '.empty', function () {
            clearSelections();
        });

        $('#oponent-info').empty().text('Ваш противник ' + gameData.oponent);
        board.css({display: 'block'});
        $('#player-info').css({display: 'block'});
    })
        .on('move', function (movement, turnOfWhite, isCheck, log) {
            if (movement.type === 'move') {
                move(movement.starId, movement.endId, movement.piece);
            }else if (movement.type === 'attack') {
                attack(movement.starId, movement.endId, movement.piece, movement.attackedPiece);
            }else if (movement.type === 'castle') {
                castle(movement.color, movement.castleType);
            }

            if (eqv(playerColor === 'white', turnOfWhite)) {
                if (isCheck) {
                    $('#player-info').empty().text('Вам шах!');
                } else {
                    $('#player-info').empty().text('Ваш ход!');
                }
            } else {
                $('#player-info').empty().text('Ход противника!');
            }

            logger(log);
        })
        .on('undoMove', function (movement, turnOfWhite, isCheck, log) {
            if (movement.type === 'move') {
                undoMove(movement.starId, movement.endId, movement.piece);
            }else if (movement.type === 'attack') {
                undoAttack(movement.starId, movement.endId, movement.piece, movement.attackedPiece);
            }else if (movement.type === 'castle') {
                undoCastle(movement.color, movement.castleType);
            }

            if (eqv(playerColor === 'white', turnOfWhite)) {
                if (isCheck) {
                    $('#player-info').empty().text('Вам шах!');
                } else {
                    $('#player-info').empty().text('Ваш ход!');
                }
            } else {
                $('#player-info').empty().text('Ход противника!');
            }

            logger(log);
        })
        .on('gameFind', function (oponent) {
            socket.emit('gameFind', oponent);
        })
        .on('loss', function (message) {
            $('#overlay').fadeIn(400, function () {
                $('#modal_form')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '50%'}, 200);
                $('#msg').empty().text(message);
            })
        });
});

function logger(serverLog){
    var log = $('#log').empty();
    serverLog.forEach(function(element, index){
        var logStr = $('<div></div>').text((index + 1) + '. ' + element);
        log.append(logStr);
    })
}

function move(startId, endId, piece) {
    clearLastMove();
    $('#' + startId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
    $('#' + endId).removeClass('empty').addClass(piece).addClass('last-move');
    clearSelections();
}

function undoMove(startId, endId, piece) {
    clearLastMove();
    $('#' + startId).removeClass('empty').addClass(piece);
    $('#' + endId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
    clearSelections();
}

function attack(startId, endId, piece, attakedPiece) {
    clearLastMove();
    $('#' + startId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
    $('#' + endId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass(piece).addClass('last-attack');
    var td = $('<td></td>').addClass(attakedPiece);
    if (td.hasClass(playerColor)) {
        $('#affected-piece').append(td);
    } else {
        $('#defited-piece').append(td);
    }
    clearSelections();
}

function undoAttack(startId, endId, piece, attakedPiece) {
    clearLastMove();
    $('#' + startId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass(piece).addClass('last-selected');
    $('#' + endId).removeClass('white black pawn rook knight bishop king queen not-moved').addClass(attakedPiece).addClass('last-attack');
    $('.' + attakedPiece).last().remove();
}

function castle(color, type){
    clearLastMove();
    if(color === 'white'){
        if(type === 'long'){
            $('#e1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#c1').removeClass('empty').addClass('white king last-move');
            $('#a1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#d1').removeClass('empty').addClass('white rook last-move');
        }else{
            $('#e1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#g1').removeClass('empty').addClass('white king last-move');
            $('#h1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#f1').removeClass('empty').addClass('white rook last-move');
        }
    }else{
        if(type === 'long'){
            $('#e8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#c8').removeClass('empty').addClass('black king last-move');
            $('#a8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#d8').removeClass('empty').addClass('black rook last-move');
        }else{
            $('#e8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#g8').removeClass('empty').addClass('black king last-move');
            $('#h8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty last-selected');
            $('#f8').removeClass('empty').addClass('black rook last-move');
        }
    }
    clearSelections();
}

function undoCastle(color, type){
    clearLastMove();
    if(color === 'white'){
        if(type === 'long'){
            $('#c1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#e1').removeClass('empty').addClass('white king');
            $('#d1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#a1').removeClass('empty').addClass('white rook');
        }else{
            $('#g1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#e1').removeClass('empty').addClass('white king');
            $('#f1').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#h1').removeClass('empty').addClass('white rook');
        }
    }else{
        if(type === 'long'){
            $('#c8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#e8').removeClass('empty').addClass('black king');
            $('#d8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#a8').removeClass('empty').addClass('black rook');
        }else{
            $('#g8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#e8').removeClass('empty').addClass('black king');
            $('#f8').removeClass('white black pawn rook knight bishop king queen not-moved').addClass('empty');
            $('#h8').removeClass('empty').addClass('black rook');
        }
    }
}

function clearSelections() {
    $('.selected').removeClass('selected');
    $('.valid-move').removeClass('valid-move');
    $('.valid-attack').removeClass('valid-attack');
    $('.valid-castle').removeClass('valid-castle');
}

function clearLastMove() {
    $('.last-selected').removeClass('last-selected');
    $('.last-move').removeClass('last-move');
    $('.last-attack').removeClass('last-attack');
}

function eqv(arg1, arg2) { //еквиваленция
    if (arg1 === true && arg2 === true) return true;
    if (arg1 === false && arg2 === false)return true;
    return false;
}
