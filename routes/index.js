var express = require('express');
var router = express.Router();
var User = require('../libs/user').User;
var checkUser = require('../libs/checkUser');

/* GET home page. */
router.get('/', checkUser, function(req, res, next) {
  res.redirect('/lobby');
});

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.post('/login', function(req, res, next) {
  var username = req.body.Login;
  var password = req.body.Password;

  if(password)
  {
    User.authorize(username, password, function(err, user){
      if(err) return next(err);
      req.session.user = user._id;
      res.redirect('lobby');
    })
  }
  else{
    res.render('login');
  }
});

router.get('/registration', function(req, res, next) {
  res.render('register');
});

router.post('/register', function(req, res, next) {
  var username = req.body.Login;
  var password = req.body.Password;

  if(password && username)
  {
    User.registration(username, password, function(err, user){
      if(err) return next(err);
      req.session.user = user._id;
      res.redirect('/login');
    })
  }
  else{
    res.render('register');
  }
});

router.post('/logout', function(req, res, next) {
  req.session.destroy();
  res.redirect('/login');
});

router.get('/lobby', checkUser, function(req, res, next) {
    res.render('lobby', {name: req.user.username});
})

router.post('/findgame', checkUser, function(req, res, next) {
  req.session.color = req.body.color;
  res.redirect('/game');
});

router.get('/game', checkUser, function(req, res, next) {
    res.render('game', {name: req.user.username});
});

module.exports = router;
